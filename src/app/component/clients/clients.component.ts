import { AfterViewInit, Component, EventEmitter, Injectable, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ClientService } from '../../service/client.service';
import { Client } from '../../model/client.model';
import { ClientCreateDialogComponent } from './client-create-dialog/client-create-dialog.component';
import { ClientInfoDialogComponent } from './client-info-dialog/client-info-dialog.component';
import { ClientEditDialogComponent } from './client-edit-dialog/client-edit-dialog.component';
import { MatSort, Sort, SortDirection } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Sortable, SortDirectionEnum } from '../../model/sortable.model';
import { SortingConstants } from '../../constant/sorting.constants';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: [ './clients.component.scss' ]
  // changeDetection: ChangeDetectionStrategy.OnPush
})
@Injectable()
export class ClientsComponent implements OnInit, AfterViewInit {

  @Output() tableUpdated: EventEmitter<any> = new EventEmitter();
  @ViewChild(MatSort) sort: MatSort;
  client: Client;
  columnsToDisplay = [ 'id', 'firstName', 'lastName', 'birthDate', 'email', 'edit', 'info' ];
  clients: Array<Client> = [];
  dataSource;

  private ngUnsubscribe = new Subject<void>();

  constructor(
    private dialog: MatDialog,
    private clientService: ClientService
  ) {
  }

  ngOnInit(): void {
    this.clientService.getClients()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(res => this.clients = res)
  }

  ngAfterViewInit(): void {
    this.dataSource = new MatTableDataSource<Client>(this.clients);
    this.sort.sortChange.subscribe((sortable: Sort) => {
      this.clientService.sort(this.buildSearchRequest(sortable.active, sortable.direction))
        .pipe(takeUntil(this.ngUnsubscribe))
        .subscribe(
          res => {
            this.clients = res
          },
          error => console.log(error));
    })
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    console.warn(typeof this.dataSource.filter)
  }

  private buildSearchRequest(orderBy: string, sortBy: SortDirection): Sortable {
    return {
      orderBy: orderBy,
      sortBy: this.buildSortDirection(sortBy)
    }
  }

  private buildSortDirection(sort: SortDirection): SortDirectionEnum {
    if (sort == SortingConstants.ASC_SORT_DIRECTION) {
      return SortDirectionEnum.ASC;
    }
    return SortDirectionEnum.DESC;
  }

  openUserEditDialog(client: Client): void {
    this.clientService.getClient(client.id)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        client => {
          this.client = client
        },
        error => console.log(error));
    this.dialog.open(ClientEditDialogComponent, {
      data: client
    });
  }

  openUserInfoDialog(client: Client): void {
    this.clientService.getClientInfo(client.id)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        client => {
          this.client = client
        },
        error => console.log(error));
    this.dialog.open(ClientInfoDialogComponent, {
      data: client
    });
  }

  openUserCreateDialog(): void {
    this.dialog.open(ClientCreateDialogComponent)
  }
}
