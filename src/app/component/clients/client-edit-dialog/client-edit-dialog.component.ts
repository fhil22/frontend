import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ClientService } from '../../../service/client.service';
import { Client } from '../../../model/client.model';

@Component({
  selector: 'app-create-user-dialog',
  templateUrl: './client-edit-dialog.component.html',
  styleUrls: [ './client-edit-dialog.component.scss' ]
})
export class ClientEditDialogComponent implements OnInit {

  clientForm: FormGroup;

  birthDate: FormControl;
  firstName: FormControl;
  lastName: FormControl;
  client: Client;

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: Client,
    private clientService: ClientService,
    public formBuilder: FormBuilder
  ) {

  }

  ngOnInit(): void {
    this.initForm();
  }

  private initForm(): void {
    this.firstName = new FormControl(this.data.firstName, Validators.compose([
      Validators.required, Validators.minLength(4) ]))
    this.lastName = new FormControl(this.data.lastName, Validators.compose([
      Validators.required, Validators.minLength(4) ]))
    this.birthDate = new FormControl(this.data.birthDate, Validators.compose(
      [ Validators.required ]))

    this.clientForm = this.formBuilder.group({
      firstName: this.firstName,
      lastName: this.lastName,
      birthDate: this.birthDate
    });
  }

  submitForm(): void {
    this.clientService.updateClient(this.buildUpdateRequest(this.data)).subscribe(
      (success) => console.log(success),
      (error) => console.log(error)
    );
  }

  private buildUpdateRequest(data: Client): Client {
    return {
      id: data.id,
      email: data.email,
      password: data.password,
      birthDate: this.birthDate.value,
      firstName: this.firstName.value,
      lastName: this.lastName.value
    }
  }
}

