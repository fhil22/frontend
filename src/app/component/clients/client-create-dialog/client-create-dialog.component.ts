import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ClientService } from '../../../service/client.service';
import { Client } from '../../../model/client.model';

@Component({
  selector: 'app-client-create-dialog',
  templateUrl: './client-create-dialog.component.html',
  styleUrls: [ './client-create-dialog.component.scss' ]
})
export class ClientCreateDialogComponent implements OnInit {
  clientForm: FormGroup;

  email: FormControl;
  password: FormControl;
  birthDate: FormControl;
  firstName: FormControl;
  lastName: FormControl;

  constructor(
    private clientService: ClientService,
    public formBuilder: FormBuilder
  ) {
  }

  ngOnInit(): void {
    this.initForm();
  }

  private initForm(): void {
    this.firstName = new FormControl(this.firstName, Validators.compose([
      Validators.required, Validators.minLength(4) ]))
    this.lastName = new FormControl(this.lastName, Validators.compose([
      Validators.required, Validators.minLength(4) ]))
    this.password = new FormControl(this.password, Validators.compose([
      Validators.required,
      Validators.minLength(8) ]))
    this.email = new FormControl(this.email, Validators.compose([
      Validators.required, Validators.email ]))
    this.birthDate = new FormControl(this.birthDate, Validators.compose(
      [ Validators.required ]))

    this.clientForm = new FormGroup({
      email: this.email,
      password: this.password,
      birthDate: this.birthDate,
      firstName: this.firstName,
      lastName: this.lastName
    });
  }

  submitForm(): void {
    this.clientService.createClient(this.buildCreateRequest()).subscribe(
      (response) => console.log(response),
      (error) => console.log(error)
    );
  }

  private buildCreateRequest(): Client {
    return {
      id: 0,
      email: this.email.value,
      password: this.password.value,
      birthDate: this.birthDate.value,
      firstName: this.firstName.value,
      lastName: this.lastName.value
    }
  }
}
