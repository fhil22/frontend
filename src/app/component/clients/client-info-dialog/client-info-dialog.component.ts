import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Client } from '../../../model/client.model';

@Component({
  selector: 'app-client-info-dialog',
  templateUrl: './client-info-dialog.component.html',
  styleUrls: [ './client-info-dialog.component.scss' ]
})
export class ClientInfoDialogComponent implements OnInit {
  carModels: string[] = [ 'BMV', 'Audi', 'Mercedes', 'Renault' ];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: Client
  ) {

  }

  ngOnInit(): void {
  }

}
