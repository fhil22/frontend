import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientInfoDialogComponent } from './client-info-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [
    ClientInfoDialogComponent
  ],
  imports: [
    CommonModule,
    MatDialogModule,
    MatButtonModule
  ]
})
export class ClientInfoDialogModule {
}
