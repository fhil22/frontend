import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { InsuranceComplex } from '../../model/InsuranceComplex';
import { ComplexService } from '../../service/complex.service';

@Component({
  selector: 'app-complex',
  templateUrl: './complex.component.html',
  styleUrls: [ './complex.component.scss' ]
})
export class ComplexComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;
  displayedColumns = [ 'id', 'duration', 'compensationPercent', 'damageLevel', 'coveredPart' ];
  complexes: Array<InsuranceComplex> = [];
  dataSource;

  constructor(
    private complexService: ComplexService
  ) {
  }

  ngOnInit(): void {
    this.complexService.getComplexes()
      .subscribe(
        complexes => this.complexes = complexes,
        error => console.log(error));
  }
}
