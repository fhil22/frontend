import { Component, OnInit, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { MatSort } from '@angular/material/sort';
import { ContractService } from '../../service/contract.service';
import { InsuranceContract } from '../../model/insurance-contract';
import { MatDialog } from '@angular/material/dialog';
import { ContractCreateDialogComponent } from './contract-create-dialog/contract-create-dialog.component';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-contract',
  templateUrl: './contract.component.html',
  styleUrls: [ './contract.component.scss' ]
})
export class ContractComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;
  displayedColumns = [ 'id', 'clientId', 'carId', 'insuranceComplexesIds' ];
  contracts: Array<InsuranceContract> = [];
  dataSource;

  private ngUnsubscribe = new Subject<void>();

  constructor(
    private matDialog: MatDialog,
    private contractService: ContractService
  ) {
  }

  ngOnInit(): void {
    this.contractService.getContracts()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        contracts => this.contracts = contracts,
        error => console.log(error));
  }

  openContractCreateDialog() {
    this.matDialog.open(ContractCreateDialogComponent)
  }

}
