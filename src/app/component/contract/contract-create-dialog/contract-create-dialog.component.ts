import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ContractService } from '../../../service/contract.service';
import { ClientService } from '../../../service/client.service';
import { CarService } from '../../../service/car.service';
import { ComplexService } from '../../../service/complex.service';
import { MatDialogRef } from '@angular/material/dialog';
import { ContractCreateDto } from '../../../model/insurance-contract';

@Component({
  selector: 'app-contract-create-dialog',
  templateUrl: './contract-create-dialog.component.html',
  styleUrls: [ './contract-create-dialog.component.scss' ]
})
export class ContractCreateDialogComponent implements OnInit {

  clientId: FormControl;
  carId: FormControl;
  complexesIds: FormControl;

  contractCreateFormGroup: FormGroup;

  clientLookups;
  carLookups;
  complexesLookups;

  constructor(private contractService: ContractService,
              private clientService: ClientService,
              private carService: CarService,
              private complexService: ComplexService,
              private dialogRef: MatDialogRef<ContractCreateDialogComponent>) {
  }

  ngOnInit(): void {
    this.loadLookups();
    this.initForm();
  }

  initForm(): void {
    this.clientId = new FormControl(this.clientId, [ Validators.required ]);
    this.carId = new FormControl(this.carId, [ Validators.required ]);
    this.complexesIds = new FormControl(this.complexesIds, [ Validators.required ]);

    this.contractCreateFormGroup = new FormGroup({
      clientId: this.clientId,
      carId: this.carId,
      complexesIds: this.complexesIds
    });
  }

  submitForm(): void {
    this.contractService.createContract(this.buildCreateRequest()).subscribe(
      (response) => {
        this.dialogRef.close(response);
      },
      (error) => console.log(error)
    );
  }

  loadLookups(): void {
    this.clientService.getLookup().subscribe(
      response => {
        this.clientLookups = response;
      },
      (error) => console.log(error)
    );
    this.carService.getLookup().subscribe(response => {
        this.carLookups = response;
      },
      (error) => console.log(error)
    );
    this.complexService.getLookup().subscribe(
      response => {
        this.complexesLookups = response;
      },
      error => console.log(error));
  }

  buildCreateRequest(): ContractCreateDto {
    return {
      clientId: this.clientId.value,
      carId: this.carId.value,
      insuranceComplexesIds: this.complexesIds.value
    };
  }

}
