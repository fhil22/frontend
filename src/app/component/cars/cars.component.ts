import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { Car } from '../../model/car.model';
import { CarService } from '../../service/car.service';
import { MatDialog } from '@angular/material/dialog';
import { CarCreateDialogComponent } from './car-create-dialog/car-create-dialog-component';
import { MatSort, Sort, SortDirection } from '@angular/material/sort';
import { Sortable, SortDirectionEnum } from '../../model/sortable.model';
import { MatTableDataSource } from '@angular/material/table';
import { SortingConstants } from '../../constant/sorting.constants';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { CarDeleteConfirmationDialogComponent } from './car-delete-confirmation-dialog/car-delete-confirmation-dialog.component';
import { CarInfoDialogComponent } from './car-info-dialog/car-info-dialog.component';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: [ './cars.component.scss' ]
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class CarsComponent implements OnInit, AfterViewInit {

  @ViewChild(MatSort) sort: MatSort;
  displayedColumns = [ 'id', 'color', 'engineCapacity', 'yearOfManufacture', 'weight', 'info', 'delete' ];
  cars: Array<Car> = [];
  dataSource;

  private ngUnsubscribe = new Subject<void>();

  constructor(
    private dialog: MatDialog,
    private carService: CarService
  ) {
  }

  ngOnInit(): void {
    this.carService.getCars()
      .pipe(takeUntil(this.ngUnsubscribe)).subscribe(
      cars => {
        this.cars = cars
      },
      error => console.log(error));
  }

  ngAfterViewInit(): void {
    this.dataSource = new MatTableDataSource<Car>(this.cars);
    this.sort.sortChange.subscribe((sortable: Sort) => {
      this.carService.sort(this.buildSearchRequest(sortable.active, sortable.direction))
        .pipe(takeUntil(this.ngUnsubscribe))
        .subscribe(
          cars => {
            this.cars = cars
          },
          error => console.log(error));
    })
  }

  private buildSearchRequest(orderBy: string, sortBy: SortDirection): Sortable {
    return {
      orderBy: orderBy,
      sortBy: this.buildSortDirection(sortBy)
    }
  }

  private buildSortDirection(sort: SortDirection): SortDirectionEnum {
    if (sort == SortingConstants.ASC_SORT_DIRECTION) {
      return SortDirectionEnum.ASC;
    }
    return SortDirectionEnum.DESC;
  }

  openCarCreateDialog(): void {
    this.dialog.open(CarCreateDialogComponent);
  }

  openCarInfoDialog(car: Car): void {
    this.carService.getCarInfo(car.id)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(car => {
        this.dialog.open(CarInfoDialogComponent, {
          data: car
        });
      }, error => console.log(error));
  }

  deleteCar(car: Car): void {
    this.carService.deleteCar(car.id)
      .subscribe(success => {
          this.dialog.open(CarDeleteConfirmationDialogComponent, {
            data: car
          }).afterClosed().subscribe(result => {
            this.cars = this.cars.filter((value, key) => {
              return value.id != car.id;
            })
          });
        }, error => console.log(error)
      );

  }

}
