import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CarService } from '../../../service/car.service';
import { Car } from '../../../model/car.model';
import { Client } from '../../../model/client.model';
import { ClientService } from '../../../service/client.service';

@Component({
  selector: 'app-cars',
  templateUrl: './car-create-dialog-component.html',
  styleUrls: [ './car-create-dialog-component.scss' ]
})
export class CarCreateDialogComponent implements OnInit {

  carForm: FormGroup
  color: FormControl;
  engineCapacity: FormControl;
  yearOfManufacture: FormControl;
  clientId: FormControl;
  weight: FormControl;
  clients: Client[];
  clientLookups;

  constructor(
    private formBuilder: FormBuilder,
    private carService: CarService,
    private clientService: ClientService
  ) {
  }

  ngOnInit(): void {
    this.clientService.getClients()
      .subscribe(
        res => {
          this.clients = res
        },
        error => console.log(error));
    this.initForm();
    this.loadLookups();
  }

  private initForm(): void {
    this.color = new FormControl(this.color, Validators.compose([
      Validators.required, Validators.minLength(3) ]))
    this.engineCapacity = new FormControl(this.engineCapacity, Validators.compose([
      Validators.required ]))
    this.yearOfManufacture = new FormControl(this.yearOfManufacture, Validators.compose([
      Validators.required, Validators.minLength(4) ]))
    this.weight = new FormControl(this.weight, Validators.compose([
      Validators.required ]))
    this.clientId = new FormControl(this.clientId);

    this.carForm = new FormGroup({
      color: this.color,
      engineCapacity: this.engineCapacity,
      yearOfManufacture: this.yearOfManufacture,
      weight: this.weight,
      clientId: this.clientId
    });
  }

  loadLookups(): void {
    this.clientService.getLookup().subscribe(
      response => {
        this.clientLookups = response;
      },
      error => console.log(error));
  }

  submitForm(): void {
    this.carService.createCar(this.buildCreateRequest())
      .subscribe(
        (response) => console.log(response),
        (error) => console.log(error))
  }

  private buildCreateRequest(): Car {
    return {
      id: 0,
      color: this.color.value,
      engineCapacity: this.engineCapacity.value,
      yearOfManufacture: this.yearOfManufacture.value,
      weight: this.weight.value,
      clientId: this.clientId.value
    }
  }

}
