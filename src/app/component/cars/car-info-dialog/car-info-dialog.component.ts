import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Car } from '../../../model/car.model';

@Component({
  selector: 'app-car-info-dialog',
  templateUrl: './car-info-dialog.component.html',
  styleUrls: [ './car-info-dialog.component.css' ]
})
export class CarInfoDialogComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: Car
  ) {
  }

  ngOnInit(): void {
  }

}
