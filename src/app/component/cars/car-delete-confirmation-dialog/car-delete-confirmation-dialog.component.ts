import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Car } from '../../../model/car.model';

@Component({
  selector: 'app-car-delete-confirmation-dialog',
  templateUrl: './car-delete-confirmation-dialog.component.html',
  styleUrls: [ './car-delete-confirmation-dialog.component.scss' ]
})
export class CarDeleteConfirmationDialogComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: Car
  ) {
  }

  ngOnInit(): void {
  }

  submitDelete() {
  }
}
