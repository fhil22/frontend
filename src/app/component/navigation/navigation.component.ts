import { Component, OnInit } from '@angular/core';
import { Client } from '../../model/client.model';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: [ './navigation.component.scss' ]
})
export class NavigationComponent implements OnInit {
  clients: Array<Client> = [];

  constructor() {
  }

  ngOnInit(): void {
  }

}
