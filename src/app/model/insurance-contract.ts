import { DateTime } from './types.model';
import { InsuranceContractClientModel } from './insurance-contract-client.model';

export class InsuranceContract {
  id: number;
  uuid: number;
  createdAt: DateTime;
  updatedAt: DateTime;

  client: InsuranceContractClientModel;
  carId: number;
  insuranceComplexIds: number[];
}

export interface ContractCreateDto {
  clientId: number;
  carId: number;
  insuranceComplexesIds: number[];
}
