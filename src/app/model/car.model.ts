export class Car {
  id: number;
  color: Color;
  engineCapacity: number;
  yearOfManufacture: number;
  weight: number;
  clientId: number;
}

export enum Color {
  RED = 'RED',
  GRAY = 'GRAY',
  BLUE = 'BLUE',
  PURPLE = 'PURPLE',
  WHITE = 'WHITE',
  BLACK = 'BLACK',
  CYAN = 'CYAN',
  ORANGE = 'ORANGE',
  LIME = 'LIME',
  AQUA = 'AQUA'
}
