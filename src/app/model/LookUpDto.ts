export interface LookupDto {
  id: number;
  label: string;
}
