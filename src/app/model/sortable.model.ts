export class Sortable {
  orderBy: string;
  sortBy: SortDirectionEnum;
}

export enum SortDirectionEnum {
  ASC,
  DESC
}
