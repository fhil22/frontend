export class Client {
  id: number;
  email: string;
  password: string;
  birthDate: number;
  firstName: string;
  lastName: string;
}
