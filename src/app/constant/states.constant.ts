export class States {
  /* tslint:disable */

  // Clients
  static readonly CLIENTS = 'clients';

  // Clients
  static readonly CARS = 'cars';

  //Complex
  static readonly COMPLEX = 'insurance-complexes';

  //Contract
  static readonly CONTRACT = 'insurance-contract';
  /* tslint:enable */
}
