export class SortingConstants {
  static readonly ASC_SORT_DIRECTION = 'asc';
  static readonly DESC_SORT_DIRECTION = 'desc';
}
