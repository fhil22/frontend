import { environment } from '../../environments/environment';

class Clients {
  static readonly base = `${ environment.serverUrl }/clients`;
  static readonly create = `${ Clients.base }`;
  static readonly sort = `${ Clients.base }/sort`;
  static readonly getLookup = `${ Clients.base }/lookup`;
  static readonly findOne = (id: number) => `${ Clients.base }/${ id }`;
  static readonly update = (id: number) => `${ Clients.base }/${ id }`;
  static readonly info = (id: number) => `${ Clients.base }/info/${ id }`;
}

class Cars {
  static readonly base = `${ environment.serverUrl }/cars`;
  static readonly create = `${ Cars.base }`;
  static readonly sort = `${ Cars.base }/sort`;
  static readonly getLookup = `${ Cars.base }/lookup`;
  static readonly info = (id: number) => `${ Cars.base }/info/${ id }`;
  static readonly delete = (id: number) => `${ Cars.base }/delete/${ id }`;
}

class Complexes {
  static readonly base = `${ environment.serverUrl }/insurance-complexes`;
  static readonly getAll = Complexes.base;
  static readonly getLookup = `${ Complexes.base }/lookup`;
  static readonly findOne = (id: number) => `${ Complexes.base }/${ id }`;
}

class Contracts {
  static readonly base = `${ environment.serverUrl }/insurance-contracts`;
  static readonly create = Contracts.base;
  static readonly getAll = Contracts.base;
  static readonly findOne = (id: number) => `${ Contracts.base }/${ id }`;
}

export class EndpointsConstant {
  static readonly CLIENTS = Clients;
  static readonly CARS = Cars;
  static readonly COMPLEXES = Complexes;
  static readonly CONTRACTS = Contracts;

}

