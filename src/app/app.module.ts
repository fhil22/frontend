import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { HttpClientModule } from '@angular/common/http';

import { NavigationModule } from './component/navigation/navigation.module';
import { ClientsModule } from './component/clients/clients.module';
import { CarsModule } from './component/cars/cars.module';
import { ClientCreateDialogModule } from './component/clients/client-create-dialog/client-create-dialog-module';
import { ClientEditDialogModule } from './component/clients/client-edit-dialog/client-edit-dialog-module';
import { ClientInfoDialogModule } from './component/clients/client-info-dialog/client-info-dialog-module';
import { CarCreateDialogModule } from './component/cars/car-create-dialog/car-create-dialog.module';
import { ContractModule } from './component/contract/contract.module';
import { CarInfoDialogModule } from './component/cars/car-info-dialog/car-info-dialog.module';
import { CarDeleteConfirmationDialogModule } from './component/cars/car-delete-confirmation-dialog/car-delete-confirmation-dialog.module';
import { ComplexModule } from './component/complex/complex.module';
import { ContractCreateDialogModule } from './component/contract/contract-create-dialog/contract-create-dialog.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    ContractCreateDialogModule,
    ComplexModule,
    CarDeleteConfirmationDialogModule,
    CarInfoDialogModule,
    ContractModule,
    CarCreateDialogModule,
    CarsModule,
    ClientInfoDialogModule,
    ClientsModule,
    ClientEditDialogModule,
    ClientCreateDialogModule,
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatToolbarModule,
    MatButtonModule,
    NavigationModule,
    HttpClientModule,
    MatDialogModule
  ],
  providers: [],
  bootstrap: [ AppComponent ]
})
export class AppModule {
}
