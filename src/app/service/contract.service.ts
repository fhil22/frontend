import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ContractCreateDto, InsuranceContract } from '../model/insurance-contract';
import { EndpointsConstant } from '../constant/endpoints.constant';

@Injectable({
  providedIn: 'root'
})
export class ContractService {

  constructor(private http: HttpClient) {
  }

  getContracts(): Observable<InsuranceContract[]> {
    return this.http.get<InsuranceContract[]>(EndpointsConstant.CONTRACTS.getAll);
  }

  createContract(createDto: ContractCreateDto): Observable<InsuranceContract> {
    return this.http.post<InsuranceContract>(EndpointsConstant.CONTRACTS.create, createDto);
  }
}
