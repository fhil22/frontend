import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { EndpointsConstant } from '../constant/endpoints.constant';
import { InsuranceComplex } from '../model/InsuranceComplex';
import { Injectable } from '@angular/core';
import { LookupDto } from '../model/LookUpDto';

@Injectable({
  providedIn: 'root'
})
export class ComplexService {
  constructor(private http: HttpClient) {
  }

  getComplexes(): Observable<InsuranceComplex[]> {
    return this.http.get<InsuranceComplex[]>(`${ EndpointsConstant.COMPLEXES.base }`);
  }

  getLookup(): Observable<LookupDto[]> {
    return this.http.get<LookupDto[]>(EndpointsConstant.COMPLEXES.getLookup);
  }
}
