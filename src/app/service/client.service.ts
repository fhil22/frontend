import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Client } from '../model/client.model';
import { EndpointsConstant } from '../constant/endpoints.constant';
import { Sortable } from '../model/sortable.model';
import { LookupDto } from '../model/LookUpDto';

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  constructor(private http: HttpClient) {
  }

  sort(sortable: Sortable): Observable<Client[]> {
    return this.http.post<Client[]>(`${ EndpointsConstant.CLIENTS.sort }`, sortable);
  }

  getClients(): Observable<Client[]> {
    return this.http.get<Client[]>(`${ EndpointsConstant.CLIENTS.base }`);
  }

  getClient(id: number): Observable<Client> {
    return this.http.get<Client>(`${ EndpointsConstant.CLIENTS.findOne(id) }`);
  }

  updateClient(client: Client): Observable<Client> {
    return this.http.post<Client>(`${ EndpointsConstant.CLIENTS.update(client.id) }`, client);
  }

  createClient(client: Client): Observable<Client> {
    return this.http.post<Client>(`${ EndpointsConstant.CLIENTS.create }`, client);
  }

  getClientInfo(id: number): Observable<Client> {
    return this.http.get<Client>(`${ EndpointsConstant.CLIENTS.info(id) }`);
  }

  getLookup(): Observable<LookupDto[]> {
    return this.http.get<LookupDto[]>(EndpointsConstant.CLIENTS.getLookup);
  }

}
