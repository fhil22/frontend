import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Sortable } from '../model/sortable.model';
import { Observable } from 'rxjs';
import { EndpointsConstant } from '../constant/endpoints.constant';
import { Car } from '../model/car.model';
import { LookupDto } from '../model/LookUpDto';

@Injectable({
  providedIn: 'root'
})
export class CarService {
  constructor(private http: HttpClient) {
  }

  getCars(): Observable<Car[]> {
    return this.http.get<Car[]>(`${ EndpointsConstant.CARS.base }`)
  }

  createCar(car: Car): Observable<Car> {
    return this.http.post<Car>(`${ EndpointsConstant.CARS.create }`, car);
  }

  sort(sortable: Sortable): Observable<Car[]> {
    return this.http.post<Car[]>(`${ EndpointsConstant.CARS.sort }`, sortable);
  }

  deleteCar(id: number): Observable<Car> {
    return this.http.get<Car>(`${ EndpointsConstant.CARS.delete(id) }`);
  }

  getCarInfo(id: number): Observable<Car> {
    return this.http.get<Car>(`${ EndpointsConstant.CARS.info(id) }`);
  }

  getLookup(): Observable<LookupDto[]> {
    return this.http.get<LookupDto[]>(EndpointsConstant.CARS.getLookup);
  }

}
