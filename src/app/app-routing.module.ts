import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { States } from './constant/states.constant';
import { ClientsComponent } from './component/clients/clients.component';
import { CommonModule } from '@angular/common';
import { CarsComponent } from './component/cars/cars.component';
import { ContractComponent } from './component/contract/contract.component';
import { ComplexComponent } from './component/complex/complex.component';

const routes: Routes = [
  {
    path: States.CLIENTS,
    component: ClientsComponent
  },
  {
    path: States.CARS,
    component: CarsComponent
  },
  {
    path: States.COMPLEX,
    component: ComplexComponent
  },
  {
    path: States.CONTRACT,
    component: ContractComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  exports: [ RouterModule ]
})
export class AppRoutingModule {
}
